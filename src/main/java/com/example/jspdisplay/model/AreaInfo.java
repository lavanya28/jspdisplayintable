package com.example.jspdisplay.model;


import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name="data")
public @Data
class AreaInfo implements Serializable {
    private static final long serialVersionUID = -2346866669992342L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;

    @Column(name = "zip")
    private Integer zip;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "latitude")
    private long latitude;

    @Column(name = "longitude")
    private long longitude;

    @Column(name = "timezone")
    private long timezone;

    @Column(name = "dst")
    private long dst;
    @Getter
    @Setter
public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public int getzip() {
        return zip;
    }
    public void sezip(int zip) {
        this.zip = zip;
    }

    public String getcity() {
        return city;
    }
    public void setcity(String city) {
        this.city = city;
    }

    public String getstate() {
        return state;
    }
    public void setstate(String state) {this.state = state; }

    public long getlatitude() {
        return latitude;
    }
    public void setlatitude(long latitude) { this.latitude = latitude; }


    public long getlongitude() {
        return longitude;
    }
    public void setlongitude(long longitude) {
        this.longitude = longitude;
    }


    public long gettimezone() {
        return timezone;
    }
    public void settimezone(long timezone) {
        this.timezone = timezone;
    }

    public long getdst() {
        return dst;
    }
    public void setdst(long dst) {
        this.dst = dst;
    }
}

