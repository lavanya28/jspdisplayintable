package com.example.jspdisplay.service;

import com.example.jspdisplay.Repository.AreaInfoRepository;
import com.example.jspdisplay.model.AreaInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class AreaInfoService {


    @Autowired
    private AreaInfoRepository areaInfoRepository;

    public List<AreaInfo> findAll() {
        List<AreaInfo> bes = (List<AreaInfo>) areaInfoRepository.findAll();
        return bes;
    }
}
