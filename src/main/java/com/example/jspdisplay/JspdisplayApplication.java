package com.example.jspdisplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JspdisplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(JspdisplayApplication.class, args);
	}

}
