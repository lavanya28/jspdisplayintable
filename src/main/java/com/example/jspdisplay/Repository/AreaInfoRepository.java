package com.example.jspdisplay.Repository;

import com.example.jspdisplay.model.AreaInfo;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AreaInfoRepository extends PagingAndSortingRepository<AreaInfo, Long>,
        JpaSpecificationExecutor<AreaInfo> {
}
