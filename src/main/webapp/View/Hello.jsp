<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<style><%@include file="/stylesheet/style.css"%></style>


<body>

<table class="table table-bordered table-striped">
<tr>
            <th>ID</th>
            <th>ZIP</th>
            <th>CITY</th>
            <th>STATE</th>
            <th>LATITUDE</th>
            <th>LONGITTUDE</th>
            <th>TIMEZONE</th>
            <th>DST</th>
        </tr>
    <c:forEach var="j" begin="0" end="43204">

        <tr>
            <td>${task[j].id}</td>
             <td>${task[j].zip}</td>
              <td>${task[j].city}</td>
               <td>${task[j].state}</td>
               <td>${task[j].latitude}</td>
               <td>${task[j].longitude}</td>
               <td>${task[j].timezone}</td>
               <td>${task[j].dst}</td>


        </tr>
    </c:forEach>
</table>
</body>
</head>
</html>